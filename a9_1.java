/*
@author: Dewei Chen 
@date: 3-10-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a9_1.java
@description: 
 
*/

public class a9_1 {

	public static void main(String[] args) {
	    fraction f1 = new fraction(9,8);
	    fraction f2 = new fraction(2,3);
	    fraction result = new fraction();
	    
	    System.out.print("The result starts off at ");
	    result.print();
	    System.out.println("");

	    System.out.print("The product of ");
	    f1.print();
	    System.out.print(" and ");
	    f2.print();
	    System.out.print(" is ");
	    result = f1.MultipliedBy(f2);
	    result.print();
	    System.out.println("");

	    System.out.print("The quotient of ");
	    f1.print();
	    System.out.print(" and ");
	    f2.print();
	    System.out.print(" is ");
	    result = f1.DividedBy(f2);
	    result.print();
	    System.out.println("");

	    System.out.print("The sum of ");
	    f1.print();
	    System.out.print(" and ");
	    f2.print();
	    System.out.print(" is ");
	    result = f1.AddedTo(f2);
	    result.print();
	    System.out.println("");

	    System.out.print("The difference of ");
	    f1.print();
	    System.out.print(" and ");
	    f2.print();
	    System.out.print(" is ");
	    result = f1.Subtract(f2);
	    result.print();
	    System.out.println("");

	    if (f1.isEqualTo(f2)){
	    	System.out.println("The two fractions are equal.");
	    } else {
	    	System.out.println("The two fractions are not equal.");
	    }
	    
	    final fraction f3 = new fraction(12, 8);
	    final fraction f4 = new fraction(202, 303);
	    result = f3.MultipliedBy(f4);
	    System.out.print("The product of ");
	    f3.print();
	    System.out.print(" and ");
	    f4.print();
	    System.out.print(" is ");
	    result.print();

	}

}
