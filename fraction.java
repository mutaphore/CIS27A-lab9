/*
@author: Dewei Chen 
@date: 3-10-2012
@class: CIS27A
@instructor: Dave Harden
@filename: fraction.java
@description: This program contains the fraction class that
represents fractions. It contains methods to add, subtract,
multiply, divide fractions as well as comparing to see if 
two fractions are equal. There's a print method that displays
the value of a fraction object in the form of numerator/denominator.
The results are always in the most reduced form.
 
*/

public class fraction {

	private int numerator;
	private int denominator;
	
	//Default constructor that sets numerator to
	//0 and denominator to 1 (to avoid indeterminate form).
	public fraction (){
		
		numerator = 0;
		denominator = 1;
		
	}
	
	
	
	
	
	
	
	
	
	//Constructor that takes in numerator and denominator as params,
	//it generates a dummy fraction called temp and calls reduceFraction()
	//that will make sure the fraction object starts off as a reduced 
	//fraction.
	public fraction (int num, int den){
		
		fraction temp = new fraction();
		
		temp.numerator = num;
		temp.denominator = den;
		
		reduceFraction(temp);
	
		numerator = temp.numerator;
		denominator = temp.denominator;
	
	}
	
	
	
	
	
	
	
	
	//Method adds the current instance of fraction object to another fraction
	//object passed as a parameter. Returns a fraction object that is the result.
	public fraction AddedTo (fraction f1){
		
		fraction result = new fraction();
		
		result.numerator = numerator*f1.denominator + f1.numerator*denominator;
		result.denominator = denominator * f1.denominator;
		
		reduceFraction(result);
		
		return result;
		
	}
	
	
	
	
	
	
	
	

	//Method subtracts the current instance of fraction object by another fraction
	//object passed as a parameter. Returns a fraction object that is the result.
	public fraction Subtract (fraction f1){
		
		fraction result = new fraction();
		
		result.numerator = numerator*f1.denominator - f1.numerator*denominator;
		result.denominator = denominator * f1.denominator;
		
		reduceFraction(result);
		
		return result;
		
	}
	
	
	
	
	
	
	
	
	

	//Method multiplies the current instance of fraction object to another fraction
	//object passed as a parameter. Returns a fraction object that is the result.
	public fraction MultipliedBy (fraction f1){
		
		fraction result = new fraction();
		
		result.numerator = numerator * f1.numerator;
		result.denominator = denominator * f1.denominator;
		
		reduceFraction(result);
		
		return result;
		
	}
	
	
	
	
	
	
	
	

	
	//Method divides the current instance of fraction object by another fraction
	//object passed as a parameter. Returns a fraction object that is the result.
	public fraction DividedBy (fraction f1){
		
		fraction result = new fraction();
		
		result.numerator = numerator * f1.denominator;
		result.denominator = denominator * f1.numerator;
		
		reduceFraction(result);
		
		return result;
		
	}
	
	
	
	
	
	
	
	
	

	//Method checks if the current instance of fraction object is equal to another fraction
	//object passed as a parameter. Creates a temp fraction object in order to make sure
	//the fraction is in reduced form. Returns true or false.
	public boolean isEqualTo (fraction f1){
		
		fraction temp = new fraction();
		boolean equal = false;
		
		temp.numerator = numerator;
		temp.denominator = denominator;
		
		reduceFraction(temp);
		reduceFraction(f1);
		
		if(temp.numerator == f1.numerator && temp.denominator == f1.denominator)
			equal = true;
		
		return equal;
		
	}
	
	
	
	
	
	
	
	
	
	//Method reduces any fraction object passed to it by reference.
	public void reduceFraction (fraction f1){
		
		int small;
		int temp;
		
		do{
			
			if (f1.numerator <= f1.denominator){ 
				small = f1.numerator;
				temp = f1.denominator;
			}
			else{
				small = f1.denominator;
				temp = f1.numerator;
			}
			
			for (int i=2 ; i <= small ; i++) {
				
				if (f1.numerator % i == 0 && f1.denominator % i == 0){
					
					f1.numerator /= i;
					f1.denominator /= i;
					temp = i;
					
				}
				
			}
			
		}while(temp <= small && (f1.numerator != 1 && f1.denominator != 1));
		
	}
	
	
	
	
	
	
	
	
	
	//Method prints the current instance of fraction object, outputs it in 
	//the form of numerator/denominator
	public void print (){
		
		System.out.print(numerator + "/" + denominator);
		
	}
	
	
}
